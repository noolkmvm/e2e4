<?php

namespace App;

use App\controllers\userController;
use App\controllers\mainController;

class Router{
    public static $routes = [
        '~^$~' => [mainController::class, 'action_index'],
        '~^login/$~' => [userController::class, 'action_login'],
        '~^auth/$~' => [userController::class, 'action_auth'],
        '~^logout/$~' => [userController::class, 'action_logout'],
        '~^add/$~' => [userController::class, 'action_addMessage'],
        '~^editing/$~' => [userController::class, 'action_editMessage'],
        '~^remove/$~' => [userController::class, 'action_delete'],
        '~^commenting/$~' => [userController::class, 'action_addComment'],
    ];

}