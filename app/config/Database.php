<?php

namespace app\config;

class Database{
    const USER = "root";
    const PASS = 'root';
    const HOST = "localhost";
    const DB   = "e2e4";

    public static function connectDatabase() {
        $user = self::USER;
        $pass = self::PASS;
        $host = self::HOST;
        $db   = self::DB;

        return new \PDO("mysql:dbname=$db;host=$host", $user, $pass);
    }

}