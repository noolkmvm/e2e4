<div class="container">
    <div class="form-signin">
        <h3>Вход</h3>
        <form action="/auth/" method="post">
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Логин:</label>
                <input type="text" class="form-control" name='login' required>
            </div>
            <div class="mb-3">
                <label class="form-label">Пароль:</label>
                <input type="password" class="form-control" name="password" required>
            </div>
            <button type="submit" class="btn btn-primary">Войти</button>
        </form>
    </div>
</div>

