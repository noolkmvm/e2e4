<?php
if(!isset($_SESSION['auth'])) {
    echo
    '<script type="text/javascript">
        location.replace("/login/");
    </script>';
}?>

<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="/">e2e4</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto mb-2 mb-lg-0">
                </ul>
                    <a type="button" class="btn btn-success" href="/logout/">Выйти</a>
            </div>
    </nav>

    <div class="p-3">
        <div type="button" class="btn btn-success" data-toggle="modal" data-target="#userAddModal">Добавить сообщение</div>
        <div class="modal fade" id="userAddModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Добавить новое сообщение</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="/add/">
                            <input type="text" name="heading" class="form-control" placeholder="Заголовок" required>
                            <textarea class="form-control" name="text" rows="5" placeholder="Содержание..." required></textarea>
                            <hr>
                            <button class="btn btn-success btn-round" type="submit">Отправить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <table class="table">
        <thead>
        <tr>
            <th scope="col" class="col-md-1"><a href="/?page=<?php echo !isset($_GET['page']) ? 1 : $_GET['page']; ?>&sort=<?php echo !isset($_GET['sort']) || $_GET['sort'] == '2' ? 1 : 2; ?>" style="color: black"># <i class="fa fa-sort" aria-hidden="true"></i></a></th>
            <th scope="col" class="col-md-1">Заголовок</th>
            <th scope="col" class="col-md-3">Краткое содержание</th>
            <th scope="col" class="col-md-4">Дествия</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($data['messages'] as $row): ?>
            <tr>
                <th scope="row"><?php echo $row['id']; ?></th>
                <td><?php echo $row['heading']; ?></td>
                <td><?php echo $row['short']; ?></td>
                <td>
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#viewingModal-<?php echo $row['id']; ?>">Прочитать</button>
                <div class="modal fade" id="viewingModal-<?php echo $row['id']; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Сообщение</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="card">
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item">Заголовок: <b><?php echo $row['heading']; ?></b></li>
                                        <li class="list-group-item">Автор: <b><?php echo $row['author']; ?></b></li>
                                        <li class="list-group-item">Краткое содержание: <b><?php echo $row['short']; ?></b></li>
                                        <li class="list-group-item">Полное содержание: <b><?=$row['text']?></b></li>
                                    </ul>
                                    <h4 class="p-3">Комментарии:</h4>
                                    <?php foreach($data['comments'] as $item): ?>
                                        <?php if($item['message_id'] == $row['id']): ?>
                                            <div class="alert alert-primary" role="alert"><b><?=$item['author']?>:</b> <?=$item['text']?></div>
                                        <?php else: ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#commentModal-<?php echo $row['id']; ?>">Добавить комментарий</button>
                <div class="modal fade" id="commentModal-<?php echo $row['id']; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Добавить комментарий</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form method="POST" action="/commenting/" id="task=<?php echo $row['id']; ?>">
                                    <input class="form-check-input" type="text" name="id" value="<?php echo $row['id']; ?>" hidden="true">
                                    <textarea class="form-control" name="text" rows="5" placeholder="Содержание..." required></textarea>
                                    <hr>
                                    <button class="btn btn-success btn-round" type="submit">Добавить</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#EditModal-<?php echo $row['id']; ?>">Редактировать</button>
                <div class="modal fade" id="EditModal-<?php echo $row['id']; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Редактировать сообщение</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form method="POST" action="/editing/" id="task=<?php echo $row['id']; ?>">
                                    <input class="form-check-input" type="text" name="id" value="<?php echo $row['id']; ?>" hidden="true">
                                    <input type="text" name="heading" class="form-control" value="<?php echo $row['heading']; ?>">
                                    <textarea class="form-control" name="text" rows="5"><?php echo $row['text']; ?></textarea>
                                    <hr>
                                    <button class="btn btn-success btn-round" type="submit">Редактировать</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-danger"><a href="/remove/?id=<?php echo $row['id']; ?>">Удалить</a></button></th>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>

<nav aria-label="Page navigation example">
    <ul class="pagination justify-content-center">
        <?php
        for($i=1; $i<= $data['countPages']; $i++) {
            echo "<li class=\"page-item\"><a class=\"page-link\" href=\"\?page={$i}\" style=\"color: black;\">{$i}</a></li>";
        }
        ?>
    </ul>
</nav>