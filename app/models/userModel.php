<?php

namespace App\models;

use App\core\Model;

class userModel extends Model{
    public function auth() {
        $login = $_POST['login'];
        $password = $_POST['password'];
        $sql = "SELECT * FROM users WHERE login = :login";

        $state = $this->db->prepare($sql);
        $state->bindParam(":login", $login);
        $state->execute();

        $user = $state->fetch(\PDO::FETCH_ASSOC);

        if(password_verify($password, $user['password'])){
            $_SESSION['auth'] = true;
            $_SESSION['login'] = $_POST['login'];
            $_SESSION['staff_status'] = $user['staff_status'];
        }else {
            $errors = 'Неверное имя пользователя или пароль';
            return $errors;
        }
    }

    public function addMessage(){
        $heading = $_POST['heading'];
        $text = $_POST['text'];
        $author = $_SESSION['login'];

        $string = strip_tags($text);
        $string = substr($string, 0, 10).'...';

        $sql = 'INSERT INTO messages (heading,short,text,author) VALUES (:heading,:short,:text,:author);';

        $state = $this->db->prepare($sql);
        $state->bindParam(":heading", $heading);
        $state->bindParam(":short", $string);
        $state->bindParam(":text", $text);
        $state->bindParam(":author", $author);
        $state->execute();
    }

    public function editMessage() {
        $id = $_POST['id'];
        $heading = $_POST['heading'];
        $text = $_POST['text'];

        $sql = "UPDATE messages SET heading = ?, text = ? WHERE id = ?";

        $state = $this->db->prepare($sql);
        $state->bindParam(1, $heading);
        $state->bindParam(2, $text);
        $state->bindParam(3, $id);
        $state->execute();
    }

    public function deleteMessage() {
        $id = $_GET['id'];
        $sql = 'DELETE FROM messages WHERE id = :id';
        $state = $this->db->prepare($sql);
        $state->bindParam(":id", $id);
        $state->execute();
    }

    public function addComment(){
        $id = $_POST['id'];
        $text = $_POST['text'];
        $author = $_SESSION['login'];

        $sql = 'INSERT INTO comments (text,message_id,author) VALUES (:text,:message_id,:author);';

        $state = $this->db->prepare($sql);
        $state->bindParam(":text", $text);
        $state->bindParam(":message_id", $id);
        $state->bindParam(":author", $author);
        $state->execute();
    }
}