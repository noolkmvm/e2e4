<?php

namespace App\models;

use App\core\Model;

class mainModel extends Model{
    public function getCountPages() {
        $limit = 3;
        $sql  = 'SELECT COUNT(*) FROM messages';
        $state = $this->db->prepare($sql);
        $state->execute();
        $tasks = $state->fetch();

        $total_page = ceil($tasks['0'] / $limit);

        return $total_page;
    }

    public function getMessagesOnPages() {
        $total_page = $this->getCountPages();

        if(!isset($_GET['page']) || intval($_GET['page']) == 0){
            $page = 1;
        }else if (intval($_GET['page']) > $total_page) {
            $page = $total_page;
        }else $page = intval($_GET['page']);

        $limit = 3;
        $offset = ($page - 1) * $limit;
        if (!isset($_GET['sort']) || $_GET['sort'] == '2'){
            $sql  = "SELECT * FROM messages order by id asc LIMIT $offset, $limit";
        }else $sql  = "SELECT * FROM messages order by id desc LIMIT $offset, $limit";

        $state = $this->db->prepare($sql);
        $state->execute();

        return $state->fetchAll();
    }

    public function getComments() {
        $sql  = 'SELECT * FROM comments';
        $state = $this->db->prepare($sql);
        $state->execute();
        $comments = $state->fetchAll();

        return $comments;
    }
}