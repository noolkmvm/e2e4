<?php

namespace App\controllers;

use App\core\Controller;
use App\core\View;
use App\models\mainModel;

class mainController extends Controller{
    function __construct(){
        $this->model = new mainModel();
        $this->view = new View();
    }

    function action_index(){
        $data['messages'] = $this->model->getMessagesOnPages();
        $data['countPages'] = $this->model->getCountPages();
        $data['comments'] = $this->model->getComments();
        $this->view->generate('mainView.php', 'template.php', $data);
    }
}