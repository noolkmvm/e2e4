<?php


namespace App\controllers;


use App\core\Controller;
use App\core\View;
use App\models\userModel;

class userController extends Controller{

    function __construct(){
        $this->model = new userModel();
        $this->view = new View();
    }

    function action_login(){
        $this->view->generate('authView.php', 'template.php');
    }

    function action_auth(){
        $this->model->auth();
        header("Location: /");
    }

    public function action_logout(){
        session_destroy();
        header("Location: /login/");
    }

    function action_addMessage(){
        $this->model->addMessage();
        header("Location: /");
    }

    function action_editMessage(){
        $this->model->editMessage();
        header("Location: /");
    }

    function action_delete(){
        $this->model->deleteMessage();
        header("Location: /");
    }

    function action_addComment(){
        $this->model->addComment();
        header("Location: /");
    }
}